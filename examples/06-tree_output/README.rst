06 - Tree output
================

.. _templator: https://gitlab.com/tomaskadlec/templator/
.. _template.html.j2: template.html.j2
.. _context.yml: context.yml
.. _run.sh: run.sh
.. _tree command: ../../docs/commands/tree.rst

This example shows how to generate tree output from `context.yml`_. Use
`run.sh`_ to try this example. It starts virtualenv, invokes templator_
with ``tree`` command. Result is stored in ``/tmp/test``.

Please refer for further information to `tree command`_.