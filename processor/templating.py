# processor/templating.py
# Classes and functions handling templating

from jinja2 import BaseLoader, Environment, TemplateNotFound
import json
from jsonpointer import resolve_pointer
from os.path import join, exists, getmtime, isdir, abspath, dirname, basename, relpath
import sys

import processor.common


class FilesystemLoader(BaseLoader):
    """
    Template loader utilizing filesystem
    """

    def __init__(self, filename):
        self.filename = filename

    def get_source(self, environment, template):
        path = self._find_template(template)
        if not path:
            raise TemplateNotFound(template)

        mtime = getmtime(path)
        with processor.common.FileReader(path) as f:
            source = f.read()
        return source, path, lambda: mtime == getmtime(path)

    def _find_template(self, template):
        paths = [
            join(self.filename, template),
            join(abspath(dirname(sys.argv[0])), template)
        ]
        for path in paths:
            if exists(path):
                return path
        return False


def _relpath(node_to, node_from, extension=None):
    """
    Jinja2 filter. Returns relative path between two directory paths
    in a tree.
    :param node_to: destination
    :param node_from: source
    :param extension: resulting extension (optional)
    :return: relative path
    """
    dir = relpath(node_to['__path'], node_from['__path'])
    if extension:
        file = "{}.{}".format(node_to['__key'], extension)
    else:
        file = node_to['__key']
    return "{}/{}".format(dir, file)


def _curpath(node):
    """
    Jinja2 filter. Returns a path in a tree from the tree root to the
    current node.
    :param node:
    :return:
    """
    curpath = list()
    while node:
        curpath.append(node)
        try:
            node = node['__parent']
        except:
            node = None
    curpath.reverse()
    return curpath


def _fqnpath(context, path_to):
    """
    Jinja2 filter. Returns a node specified by its FQN.
    :param context: tree
    :param path_to: FQN
    :return:
    """
    return resolve_pointer(context, '/' + path_to.replace('.', '/'), False)


def _to_json(node):
    """
    Jinja2 filter. Returns an object serialized in JSON format
    :param node:
    :return:
    """
    return json.dumps(node)


env = None


def get_templating(config_path=None, template_path=None):
    global env

    if not env:
        config = processor.common.configure(config_path, 'jinja2')
        env = Environment(
            loader=FilesystemLoader(dirname(template_path)),
            **config
        )
        env.filters['relpath'] = _relpath
        env.filters['curpath'] = _curpath
        env.filters['fqnpath'] = _fqnpath
        env.filters['to_json'] = _to_json
        env.filters['to_yaml'] = _to_json

    return env


def get_template(template_path, config_path):
    """
    Returns a configured template
    :param template_path: Path to a template
    :param config_path: (optional) Path to a config file
    :return: configured Jinja2 template
    """
    env = get_templating(config_path, template_path)

    return env.get_template(basename(template_path))
