# processor/output.py
# Classes handling output processing

from fs.osfs import OSFS
import fs.errors
from os.path import join, exists, getmtime, isdir, abspath, dirname, basename, relpath

import processor.templating


class TreeWriter(object):

    def __init__(self, context, template, extension, build_dir,
                 branching=('type', 'node'), ordering=None, root=''):
        super().__init__()

        self.linear = list()

        self.context = context
        self.template = template
        self.extension = extension
        self.attributes = list()
        for attr, value in branching:
            self.attributes.append((attr.strip(' \t\r\n"\''), value.strip(' \t\r\n"\'')))
        self.ordering = ordering

        if not isinstance(build_dir, OSFS):
            self.build_dir = OSFS(build_dir, create=True)
        else:
            self.build_dir = build_dir

        self.root = root
        self.prev = None
        self.next = None

    def write(self):
        self._linearize()
        for node in self.linear:
            context = self.context
            if self.root:
                context = self.context[self.root]
            result = self.template.render({'node': node, 'context': context, 'extension': self.extension})
            try:
                self.build_dir.makedir(node['__path'])
            except fs.errors.DirectoryExists:
                pass
            self.build_dir.settext("{}/{}.{}".format(node['__path'], node['__key'], self.extension), result)

    def _linearize(self, key='', node=None, parent=None, path='', level=0):
        # init node for the topmost level
        node, key = self._init(node, key)
        # no way to process these nodes
        if not isinstance(node, dict) or (isinstance(key, str) and key[0] == '_'):
            return
        # remember information about self and neighbours
        self._meta(node, **{'__key': key, '__parent': parent, '__level': level, '__path': path if path else '/',
                            '__leaf': True})
        # append
        self.linear.append(node)
        # check if a branching attribute exists and has appropriate value
        recurse = False
        for (attr, value) in self.attributes:
            if attr in node and node[attr] == value:
                recurse = True
                node['__leaf'] = False
                break
        # recurse if requested (branching attribute)
        if recurse:
            node['__children'] = list()
            for sub_key in self._order(node):
                self._linearize(sub_key, node[sub_key], node,
                                "{}/{}".format(path, key),
                                level+1)

    def _init(self, node, key):
        """
        Initializes data structures if empty
        :param node: None or dict
        :param key: string (may be empty)
        :return:
        """
        if node is None and key == '' and isinstance(self.context, dict):
            if self.root:
                node = self.context[self.root]
                key = self.root
            else:
                node = self.context
                key = 'root'
        return node, key

    def _meta(self, node, **kwargs):
        """
        Sets key-value pairs to a dict.
        :param node: a dict
        :param kwargs: key-value pairs
        """
        for key, value in kwargs.items():
            node[key] = value
        # parent_path (relative)
        if node['__parent']:
            node['__parent_path'] = relpath(node['__parent']['__path'], node['__path'])
            node['__parent']['__children'].append(node)
        # prev_path (relative)
        node['__prev'] = self.prev
        if self.prev:
            node['__prev_path'] = relpath(self.prev['__path'], node['__path'])
        # next and next_path (relative)
        node['__next'] = None
        if isinstance(self.prev, dict):
            self.prev['__next'] = node
            self.prev['__next_path'] = relpath(node['__path'], self.prev['__path'])
        # make current prev for next step
        self.prev = node

    def _order(self, node):
        """
        Order dict members by an attribute or by a key.
        :param node: dict to order
        :return: ordered list of keys
        """
        if self.ordering:
            sub_keys = list()
            for sub_key, sub_value in node.items():
                try:
                    sub_keys.append((sub_key, sub_value[self.ordering]))
                except:
                    sub_keys.append((sub_key, 0))

            sub_keys = map(lambda item: item[0], sorted(sub_keys, key=lambda item: (item[1], item[0])))

        else:
            sub_keys = sorted(node.keys())

        return sub_keys
