templator
=========

.. _templator: https://gitlab.com/tomaskadlec/templator
.. _Python 3: https://www.python.org/
.. _Jinja 2: http://jinja.pocoo.org/docs/dev/
.. _Restructured Text: http://docutils.sourceforge.net/rst.html
.. _Tex: https://www.tug.org/begin.html
.. _LaTeX: https://www.latex-project.org/
.. _pyyaml: http://pyyaml.org/wiki/PyYAMLDocumentation
.. _pykwalify: http://pykwalify.readthedocs.io/en/master/
.. _YAML: http://yaml.org/
.. _tree: https://en.wikipedia.org/wiki/Tree_(graph_theory)

.. warning:: **This tool is no longer developed**. 

   There is a replacement project `docgen <https://gitlab.com/tomaskadlec/docgen>`_ 
   that can be used instead of templator_ with little effort (i.e. some templator 
   features must be implemented as extensions).

Templator_ is a simple tool written in `Python 3`_ that allows to transform
structured data (a tree to be ) using a template (or templates) to a desired result.
Currently, it supports:

* YAML_ version 1.1 as format for structured data,
* `Jinja 2`_ as a templating engine.

Result may be whatever you like - `ReStructured Text`_, TeX_ or LaTeX_, HTML,
plain text file etc.

.. _`MIT license`: LICENSE

Templator_ is licensed under `MIT license`_ (full text is distributed with the
project in a file called ``LICENSE`` in the root folder of the project).

Features
--------

input - called context

* load multiple files
* load directories recursively looking for \*.yml files
* validate YAML

  *  using pykwalify_

* scope structured data according filesystem path
* prepend a path prefix to loaded data
* evaluate expressions using `Jinja 2`_ (loaded context is dumped to YAML,
  processed by `Jinja 2`_ and parsed again)
* dump loaded data structure to STDERR

output - templates

* create a single output stream - to STDOUT
* tree output

  * preprocess the tree for navigation (parents, children, next/previous
    - based on a depth-first search algorithm)
  * helpers for walking the tree (filters, macros)
  * create a directory hierarchy based on structured data (one or mode branching
    attribute and its value is required)

Documentation
-------------

.. _installation instructions: docs/installation.rst
.. _invocation: docs/invocation.rst
.. _structured data: docs/data.rst
.. _authoring templates: docs/templating.rst
.. _run command: docs/commands/run.rst
.. _tree command: docs/commands/tree.rst

Please refer to the following documents for further information:

#. `Installation instructions`_
#. `Invocation`_ of templator_
#. Preparing `Structured data`_
#. `Authoring templates`_
#. Commands

   #. Single stream output - `run command`_
   #. Tree output - `tree command`_

.. _examples: examples/README.rst

If you want to see examples take a peek into the `examples`_ folder.