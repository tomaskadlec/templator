#!/usr/bin/env python3
# Executes commands

import click
from pykwalify.errors import SchemaError
from os.path import abspath, dirname
import sys
import yaml

import processor.common
from processor.input import get_context
import processor.output
from processor.templating import get_template, get_templating
from processor.extensions import load


@click.group()
def cli():
    sys.path.append(abspath(dirname(sys.argv[0])))


@cli.command()
@click.argument('template')
@click.argument('contexts', nargs=-1, required=True)
@click.option('--config', default=None, help="Path to configuration file")
@click.option('--dump/--no-dump', default=False, help="Dump context to STDERR")
@click.option('--expressions/--no-expressions', default=True, help="Parse Jinja2 expressions in context or not")
@click.option('--schema', default=None, multiple=True,
              help="Validate resulting context using pykwalify schema. May be provided multiple times.")
@click.option('--scope-root', default=None, multiple=True,
              help="If given attributes are scoped using directory path left between scope root and file.")
@click.option('--root', default=None, help="Set context root")
@click.option('--offset', default=None, help="Use specified offset as a context.")
@click.option('--reeval/--no-reeval', default=True, help="Reevaluate strings as YAML after running Jinja to allow some dirty hacks.")
@click.option('--extension', default=None, multiple=True, help="Load module as templator extension")
@click.option('--extension-path', default=None, multiple=True, help="Path to look for extension modules")
@click.option('--extension-argument', default=None, multiple=True, help="Argument for extension modules")
def run(contexts, template, config, dump, expressions, schema, scope_root, root, offset, reeval,
        extension, extension_path, extension_argument):
    """
    Outputs a template rendered using contexts to STDOUT.

    CONTEXTS is one or more files/directories. If a directory is given it is
    searched for *yml files recursively. All files are loaded in the same order
    as they appear on the commandline or based on their order in directories.
    """

    get_templating(config, template)
    load(extension, extension_path, extension_argument)

    try:
        context = get_context(contexts, expressions, schema, scope_root, root, reeval)
        if dump:
            click.echo(yaml.dump(context), err=True)
        if offset:
            for path in offset.split('.'):
                context = context[path]
        template = get_template(template, config)
        click.echo(template.render(context))
    except SchemaError as ex:
        click.echo("\n{}".format(ex.msg), err=True)
        exit(1)


@cli.command()
@click.argument('build_dir')
@click.argument('template')
@click.argument('contexts', nargs=-1, required=True)
@click.option('--config', default=None, help="Path to configuration file")
@click.option('--dump/--no-dump', default=False, help="Dump context to STDERR")
@click.option('--expressions/--no-expressions', default=True, help="Parse Jinja2 expressions in context or not")
@click.option('--schema', default=None, multiple=True,
              help="Validate resulting context using pykwalify schema. May be provided multiple times.")
@click.option('--scope-root', default=None, multiple=True,
              help="If given attributes are scoped using directory path left between scope root and file.")
@click.option('--root', default=None, help="Set context root")
@click.option('--branching-attribute', default=('type', 'node'), nargs=2, multiple=True,
              help="Name of attribute that is used to branching ")
@click.option('--ordering-attribute', default=None, help="Name of attribute used to order dictionaries")
@click.option('--extension', default=None, multiple=True, help="Load module as templator extension")
@click.option('--extension-path', default=None, multiple=True, help="Path to look for extension modules")
@click.option('--extension-argument', default=None, multiple=True, help="Argument for extension modules")
def tree(build_dir, contexts, template, config, dump, expressions, schema, scope_root, root,
         branching_attribute, ordering_attribute,
         extension, extension_path, extension_argument):
    """
    Outputs a tree structure of templates rendered with contexts to build_dir.

    CONTEXTS is one or more files/directories. If a directory is given it is
    searched for *yml files recursively. All files are loaded in the same order
    as they appear on the commandline or based on their order in directories.
    """

    get_templating(config, template)
    load(extension, extension_path, extension_argument)

    template_extension = template.split('.')[-2]
    template = get_template(template, config)
    try:
        context = get_context(contexts, expressions, schema, scope_root, root)
        if dump:
            click.echo(yaml.dump(context), err=True)

        # TreeWriter
        tree_writer = processor.output.TreeWriter(context, template, template_extension, build_dir,
                                                  branching=branching_attribute,
                                                  ordering=ordering_attribute,
                                                  root=root)
        tree_writer.write()
    except SchemaError as ex:
        click.echo("\n{}".format(ex.msg), err=True)
        exit(1)

if __name__ == '__main__':
    cli()
