07 - Complex strings
================

.. _Restructured Text: http://docutils.sourceforge.net/rst.html
.. _context.yml: context.yml
.. _template.rst.j2: template.rst.j2
.. _run.sh: run.sh

This is a regression test for strings with YAML special characters.
Previous versions of Templator re-interpreted string values as YAML, resulting in unpredictable errors for values with colons and other special characters.

  * context - context.yml_
  * template - template.rst.j2_

Use run.sh_ to try this example. It starts virtualenv, invokes templator, stores
result in ``/tmp/07-complex_strings.rst`` and deactivates virtualenv.
