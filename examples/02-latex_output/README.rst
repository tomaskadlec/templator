02 - LaTeX output
=================

.. _LaTeX: https://www.latex-project.org/
.. _context.yml: context.yml
.. _template.tex.j2: template.tex.j2
.. _config.ini: config.ini
.. _run.sh: run.sh

This is a basic demonstration how to use templator with `LaTeX`_.

  * context - context.yml_
  * template - template.tex.j2_
  * configuration - config.ini_

Use run.sh_ to try this example. It starts virtualenv, invokes templator, stores
result in ``/tmp/02-latex_output.tex`` and deactivates virtualenv.
