Single stream output
====================

::

  Usage: templator.py run [OPTIONS] TEMPLATE CONTEXTS...

      Outputs a template rendered using contexts to STDOUT.

      CONTEXTS is one or more files/directories. If a directory is given it is
      searched for *yml files recursively. All files are loaded in the same
      order as they appear on the commandline or based on their order in
      directories.

  Options:
      --config TEXT                   Path to configuration file
      --dump / --no-dump              Dump context to STDERR
      --expressions / --no-expressions
                                      Parse Jinja2 expressions in context or not
      --schema TEXT                   Validate resulting context using pykwalify
                                      schema. May be provided multiple times.
      --scope-root TEXT               If given attributes are scoped using
                                      directory path left between scope root and
                                      file.
      --root TEXT                     Set context root
      --help                          Show this message and exit.

Preprocessing
-------------

Data are not preprocessed.

Filters
-------

No specific filters are defined.

Macros
------

No specific macros are defined.

Examples
--------

Examples related to single stream output:

  * `01 - Basic usage <../../examples/01-basic_usage/README.rst>`_
  * `02 - LaTeX output <../../examples/02-latex_output/README.rst>`_
  * `03 - Split context <../../examples/03-split_context/README.rst>`_
  * `04 - Schema validation <../../examples/04-schema_validation/README.rst>`_
  * `05 - Scoped attributes <../../examples/05-scoped_attributes/README.rst>`_
