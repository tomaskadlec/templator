Invocation
==========

.. _Jinja 2: http://jinja.pocoo.org/docs/dev/
.. _pyyaml: http://pyyaml.org/wiki/PyYAMLDocumentation
.. _templator: https://gitlab.com/tomaskadlec/templator
.. _`templator.sh`: ../templator.sh
.. _virtualenv: https://virtualenv.pypa.io/en/stable/

.. warning:: Don't forget to activate virtualenv_ prior running the tool or use
   provided wrapper `templator.sh`_ that does it.

::

    tomas@pc.tomaskadlec.net:templator$ templator --help
    Usage: templator [OPTIONS] COMMAND [ARGS]...

    Options:
      --help  Show this message and exit.

    Commands:
      run   Outputs a template rendered using contexts to...
      tree  Outputs a tree structure of templates...

Structured data to render are called *context*. *Context* can be one file, multiple
files or a directory that is recursively  searched for files ending with ``*.yml``
extension. Files that are loaded must be valid YAML files. Trees loaded from multiple
files are merged together. The latter has always higher priority thus overwrites the
former one.

You may use syntax supported by pyyaml_ module that is used to parse YAML (e.g.
references): ::

  # context.yml

  persons:
    john: &author
      name: John Example

  document:
    title: Example document
    author: *author

.. _FilesystemLoader: processor/output.py

*Template* is a document to render. It may contain expressions that will be
evaluated using *context*. *Templates* are processed by `Jinja 2`_. It is possible
to use all of its features including inheritance (``extend``) or including
(``include``) other template files. These additional files must be referenced using
relative path to the main template (thus FilesystemLoader_ is able to find them). It
is recommended to place them in the same directory as the main template. ::

  {# template.txt.j2 #}
  # {{document.title}}

  {{author.name}}

  Text ...


To create a result just invoke templator_: ::

  tomas@pc.tomaskadlec.net:templator$ templator run template.txt.j2 context.yml

Using templator_ in more complicated scenarios is described later in the documentation.
