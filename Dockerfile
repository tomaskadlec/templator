FROM alpine:latest

ENV LANG C.UTF-8
ENV LANGUAGE C.UTF-8
ENV LC_ALL C.UTF-8

ENV GOSU_VERSION 1.10
RUN \
    apk add --no-cache --virtual .gosu-deps \
        dpkg \
        openssl \
    && dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" \
    && wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" \
    && chmod +x /usr/local/bin/gosu \
    && gosu nobody true \
    && apk del .gosu-deps

RUN apk add --no-cache \
        bash \
        python3

ENV TEMPLATOR_VENV no

RUN mkdir -p /usr/local/templator

ADD ["requirements.txt", "/usr/local/templator/"]

RUN \
    apk add --no-cache --virtual .templator-deps gcc musl-dev python3-dev \
    && pip3 install --upgrade pip \
    && pip3 install -r /usr/local/templator/requirements.txt \
    && apk del .templator-deps

ADD ["LICENSE", "templator.*", "/usr/local/templator/"]
ADD ["macro", "/usr/local/templator/macro"]
ADD ["processor", "/usr/local/templator/processor"]

RUN ln -s /usr/local/templator/templator.sh /usr/local/bin/templator

ADD ["init.sh", "*.init.sh", "/usr/local/container_init/"]
RUN chmod +x /usr/local/container_init/init.sh
ENTRYPOINT ["/bin/sh", "/usr/local/container_init/init.sh"]
CMD ["/bin/sh"]
