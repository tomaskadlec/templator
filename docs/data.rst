Structured data
===============

.. _templator: https://gitlab.com/tomaskadlec/templator
.. _Jinja 2: http://jinja.pocoo.org/docs/dev/

Templator_ expects data to be in YAML format. However, it is able to read data
in several ways:

  * load one or more multiple files,
  * walk directory structure recursively and load all files with ``*.yml`` extension.

Files are processed in the order they appear on the command line or are discovered in
directory structure. Each file is loaded, parsed and merged into already loaded data.
That means that files processed later have higher priority than files processed earlier.
Thus priority grows from left to right.

There are several options that can modify behavior of input processing.

Root node
---------

It might be handy sometimes that you can specify a root node (or path) to which everything
is appended. Consider following example: ::

  property1: value
  property2: value
  ...
  propertyN: value

There is no way how to iterate through all properties in  `Jinja 2`_. There is no option to
iterate through all the properties defined at context root. However, it is possible using
templator_ ``--root`` option to make all such nodes ancestors of a given node (or node path).
under which all properties are appended. ::

  templator --root data template.txt.j2

This root node (or node path) can be used latter in templates. ::

 {% for property, value in data.items() %}
     {{ property }}: {{value}}
 {% endfor %}

Scoping
-------

It is complicated sometimes to structure data well. You must remember the level you are working on
and indent data properly. It might help to store it in appropriate directory structure. Templator_
allows to use directory structure to scope the nodes. Templator_ just needs to know which part of
filesystem path it should use. This is handled by ``--scope-root`` option. It expects path prefix
that will be stripped from path to the file. The rest of the path will be used to scope nodes read
from that file. Note that directories only are used for scoping nodes. Option may be given more
than once because multiple files or directories may be specified at a time. Consider following example: ::

  .
  -- category
      -- a.yml
         product_a:
           title: A

To access ``title`` in templating use ``category.product_a.title``.

Validation
----------

.. _pykwalify:  http://pykwalify.readthedocs.io/en/unstable/

Templator_ supports validation of processed data.  Validation is handled by pykwalify_. Just use
``--schema`` option and provide path to your schema. Option may be used multiple times.
