# base/init.d/10-base-user.init.sh
# Creates user/group

# Handle group
if [ -z "$CONTAINER_GID" ]; then
    export CONTAINER_GID=$(getent group 'nobody' | cut -d: -f 3)
elif ! getent group "$CONTAINER_GID"; then
    addgroup -g "$CONTAINER_GID" container_group
fi
export CONTAINER_GROUP=$(getent group "$CONTAINER_GID" | cut -d: -f1)

# Handle user
if [ -z "$CONTAINER_UID" ]; then
    export CONTAINER_UID=$(getent passwd 'nobody' | cut -d: -f 3)
elif ! getent passwd "$CONTAINER_UID"; then
    adduser -G "$CONTAINER_GROUP" -u "$CONTAINER_UID" -D container_user
fi
export CONTAINER_USER=$(getent passwd "$CONTAINER_UID" | cut -d: -f 1)

