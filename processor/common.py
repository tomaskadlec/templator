# processor/common.py
# Common constructs

import configparser
from os.path import abspath


class FileReader(object):
    """
    Opens and reads a file
    """
    def __init__(self, filename):
      self.file = open(filename)

    def __enter__(self):
        return self.file

    def __exit__(self, ctx_type, ctx_value, ctx_traceback):
        self.file.close()


def configure(config, key):
    """
    Configures an application and returns requested config key
    or an empty dict.
    :param config:
    :param key:
    :return:
    """
    if config is not None:
        c = configparser.ConfigParser()
        c.read(abspath(config))
        if key in c.sections():
            return c[key]
    return {}
