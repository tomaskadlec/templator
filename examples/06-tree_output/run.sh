#!/bin/sh

SCRIPT_DIR=$(dirname "$0")


. "$SCRIPT_DIR/../../.venv/bin/activate"
python "$SCRIPT_DIR/../../templator.py" tree \
    --dump \
    --branching-attribute type node \
    -- \
    /tmp/test \
    "$SCRIPT_DIR/template.html.j2" \
    "$SCRIPT_DIR/context.yml"

deactivate
