08 - Extensions
===============

.. _Restructured Text: http://docutils.sourceforge.net/rst.html
.. _context.yml: context.yml
.. _template.rst.j2: template.rst.j2
.. _run.sh: run.sh

This is a demonstration how to create extensions for templator. This example is based on the first one. The extension
has two parts:

* ``context_ready`` event handler - it modifies context when it is ready;
* Jinja2 filter called ``example`` which replaces any value given to it with string ``EXAMPLE``.

Files
-----

* context - context.yml_
* template - template.rst.j2_

Usage
-----

Use run.sh_ to try this example. It

* starts virtualenv,
* invokes templator,
* prints result to ``STDOUT``
* and deactivates virtualenv.
