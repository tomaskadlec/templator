"""
Loads extensions
"""
from importlib import import_module
from os.path import abspath
from processor.templating import get_templating
import sys


handlers = {
    'context_ready': []
}


# https://docs.python.org/3/library/importlib.html#importlib.import_module
# http://stackoverflow.com/questions/951124/dynamic-loading-of-python-modules
def load(extensions, extension_paths, extension_arguments):
    """
    Load all extensions
    :param extensions:
    :return:
    """
    global handlers

    if not isinstance(extensions, list):
        extensions = list(extensions)

    if not isinstance(extension_arguments, list):
        extension_arguments = list(extension_arguments)

    _load_paths(extension_paths)

    for extension in extensions:
        module = import_module(extension)
        module.register(handlers=handlers, templating=get_templating(), arguments=extension_arguments)


def _load_paths(extension_paths):

    if not isinstance(extension_paths, list):
        extension_paths = list(extension_paths)

    for extension_path in extension_paths:
        extension_path = abspath(extension_path)
        sys.path.append(extension_path)