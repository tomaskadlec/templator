03 - Split context
==================

.. _Restructured Text: http://docutils.sourceforge.net/rst.html
.. _context.yml: context.yml
.. _context_high.yml: context_high.yml
.. _template.rst.j2: template.rst.j2
.. _run.sh: run.sh

This is a basic demonstration how use and combine more contexts. Mind that later has more priority.

  * context - context.yml_, context_high.yml_
  * template - template.rst.j2_

Use run.sh_ to try this example. It starts virtualenv, invokes templator, stores
result in ``/tmp/03-split_context.rst`` and deactivates virtualenv.
