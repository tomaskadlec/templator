# processor/input.py
# Classes handling input processing
# Inspired by http://stackoverflow.com/a/21048064

import click
import collections
import fnmatch
from jinja2 import Template
import json
from jsonpointer import resolve_pointer, set_pointer
import os.path
import pykwalify.core
import yaml
import yaml.resolver

from processor.common import FileReader
from processor.extensions import handlers
from processor.templating import get_templating

_mapping_tag = yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG


def _dict_representer(dumper, data):
    return dumper.represent_dict(data.items())


def _dict_constructor(loader, node):
    return collections.OrderedDict(loader.construct_pairs(node))


def parse(content, path):
    try:
        yaml.add_representer(collections.OrderedDict, _dict_representer)
        yaml.add_constructor(_mapping_tag, _dict_constructor)
        return yaml.load(content)
    except Exception as ex:
        click.echo("Path: {}\n{}".format(path, ex), err=True)


def get_context(paths, expressions=True, schema=None, scope_root=None, root=None, reeval=None):
    """
    Loads context from provided paths. If expressions is True
    - a template (yaml.dump) is created from the current context
    - the template it rendered by Jinja2 with the current context as a context
    - rendered template is parsed from YAML to a final context (yaml.load)

    :return context
    """
    global handlers
    context = {}
    # convert scope roots to absolute paths
    if scope_root:
        scope_root = [os.path.realpath(p) for p in scope_root]
    else:
        scope_root = []
    # walk all given paths recursively
    for path in paths:
        if os.path.isdir(path):
            context = _merge(context, _load_dir(path, scope_root))
        elif os.path.isfile(path):
            context = _merge(context, _load_file(path, scope_root))
        else:
            raise RuntimeError('Path {} is neither directory nor file.')
    # evaluate jinja2 expressions in context
    if expressions:
        _evaluate(context, context, reeval)
    # validate resulting data structure
    if schema:
        _validate(context, schema)
    # set context root
    if root:
        rooted_context = collections.OrderedDict()
        rooted_context[root] = context
        return rooted_context
    # run handlers
    for handler in handlers['context_ready']:
        handler(context)
    # hurray
    return context


def _load_dir(path, scope_root):
    context = {}
    for root, dirnames, filenames in os.walk(path):
        for filename in fnmatch.filter(filenames, '*.yml'):
            try:
                context = _merge(context, _load_file(os.path.join(root, filename), scope_root))
            except UnexpectedResultError as ex:
                click.echo(ex, err=True)
        for dirname in dirnames:
            context = _merge(context, _load_dir(dirname, scope_root))
    return context


def _load_file(path, scope_root):
    with FileReader(path) as f:
        result = parse(f.read(), path)

    if not isinstance(result, dict):
        raise UnexpectedResultError(path)

    # scoped attributes
    if scope_root:
        path = os.path.realpath(path)
        prefix = os.path.commonprefix([path, *scope_root])
        if prefix:
            path = os.path.dirname(path[len(prefix)+1:])
            if path:
                keys = path.split(os.path.sep)
                keys.reverse()
                return _init_dict_recursive(keys, result)

    return result


def _init_dict_recursive(keys, value, d=None):
    if not isinstance(d, dict):
        d = collections.OrderedDict()
    key = keys.pop()
    if not keys:
        d[key] = value
    else:
        if key not in d:
            d[key] = dict()
        _init_dict_recursive(keys, value, d[key])
    return d


def _merge(original, update):
    """
    Recursively merges dict update into original dict.
    """
    for key, value in update.items():
        if isinstance(value, dict) and key in original and isinstance(original[key], dict):
            original[key] = _merge(original[key], value)
        else:
            original[key] = value
    return original


def _validate(context, schema):
    if not isinstance(schema, list):
        schema = list(schema)
    try:
        validator = pykwalify.core.Core(source_data=context, schema_files=schema)
        validator.validate(raise_exception=True)
    except Exception as ex:
        raise ex


def _evaluate(current, context, reeval=None, path=''):
    """
    Depth-first traversal of the context tree. Each str node is evaluated as
    a Jinja2 template.
    :param current:
    :param context:
    :return:
    """
    try:
        if isinstance(current, dict):
            for key in current.keys():
                result = _evaluate(current[key], context, reeval, path=path + '.' + key)
                if result is not None:
                    current[key] = result
            return None

        elif isinstance(current, str):
            template = current.strip(' \t\r\n"\'')
            if reeval:
                template = yaml.dump(template)
            template = get_templating().from_string(template)
            result = template.render(context)
            if result == current:
                return None

            result = result.strip(' \t\r\n"\'')
            if reeval:
                result = yaml.load(result)
            return result

        return None
    except Exception as e:
        click.echo("Failed to _evaluate {}".format(path))
        click.echo(e)


class UnexpectedResultError(Exception):
    message = "Failed to load {}. Result should be a dict."

    def __init__(self, path):
        super().__init__(self.message.format(path))
