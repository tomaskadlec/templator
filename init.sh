#!/bin/sh
# Handles container initialization

INIT_PATH=$(readlink -f "$0")
INIT_DIR=$(dirname "$INIT_PATH")

# run all init scripts
for INIT_SCRIPT in "$INIT_DIR/"*.init.sh; do
    if [ -f "$INIT_SCRIPT" ]; then
        echo "container_init: sourcing $INIT_SCRIPT"
        source "$INIT_SCRIPT"
    fi
done

# run CMD or user defined command
echo "container_init: GOSU $CONTAINER_USER($CONTAINER_UID) $CONTAINER_GROUP($CONTAINER_GID)"
echo "container_init: CMD $@"
exec gosu "$CONTAINER_USER" "$@"
