#!/bin/sh

SCRIPT_DIR=$(dirname "$0")


. "$SCRIPT_DIR/../../.venv/bin/activate"
python "$SCRIPT_DIR/../../templator.py" run --dump "$SCRIPT_DIR/template.rst.j2" "$SCRIPT_DIR/context.yml" > "/tmp/01-basic_usage.rst"

deactivate
