"""
examples/08-extensions/extension.py
"""

import click
import datetime


def register(handlers, templating, arguments):
    """
    Registration function (required, templator calls this function)

    :param handlers: dict of lists of handlers, key is name of an event
    :param templating: templating engine
    :param arguments: command line arguments
    :return:
    """
    # register handler
    handlers['context_ready'].append(_register_context_extension(arguments))
    # register templating filter
    templating.filters['example'] = _example_filter
    click.echo('Registered')


def _register_context_extension(arguments):
    """
    Handler registration. This function parses the arguments first and then
    defines and returns an actual handler.

    :param arguments: Arguments given on command line
    :return: closure that will be registered as event handler
    """
    # parse command line arguments to local variables
    for argument in arguments:
        try:
            (key, value) = argument.split('=')
            if key == 'example:test':
                test = value
        except:
            continue

    # this is the handler
    def _example_handler(context):
        context['created'] = "{}".format(datetime.datetime.now().isoformat())
        context['argument'] = test

    # return the handler as a closure
    return _example_handler


def _example_filter(value):
    """
    Jinja2 filter

    :param value: a value to filter
    :return: Returns string EXAMPLE always
    """
    return 'EXAMPLE'
