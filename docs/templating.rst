Authoring templates
===================

.. _Jinja 2: http://jinja.pocoo.org/docs/dev/
.. _Template Designer Documentation: http://jinja.pocoo.org/docs/dev/templates/
.. _templator: https://gitlab.com/tomaskadlec/templator
.. _Tex: https://www.tug.org/begin.html
.. _LaTeX: https://www.latex-project.org/

Templator_ uses `Jinja 2`_ templating engine. Please follow `Template Designer Documentation`_
when authoring templates. Different approaches are used to pass *context* to
templating engine environment.

The first one passes the *context* as is thus all its properties become variables
that can be used directly. Consider following example: ::

  # context.yml
  document:
    title: Title

::

  {# template.txt.j2 #}

  {{document.title}}

The second approach passes a dictionary that contains properties ``node`` a node that
is being visited and a ``context`` the whole *context*.

Please check documentation for command used which approach is used to prepare template
accordingly.

Helpers
-------

Templator_ offers a few generic helpers that ease template development. Filters are loaded
automatically. Macros might be imported from ``macro`` subdirectory. Additional helpers might
be provided for use with a particular command. Please refer to command documentation for
further instructions.

Special cases
-------------

TeX, LaTeX and co.
~~~~~~~~~~~~~~~~~~

If TeX_, LaTeX_ or similar are used as output format `Jinja 2`_ default tokens
denoting start/end of a block or variable may clash with directives and macros.
It is necessary to redefine them. It is done by providing a configuration
to templator_. It is a INI style configuration file. An example listing all possible
options is shown bellow.

Please note that no configuration is needed if default tokens are used.

::

  [jinja2]
  block_start_string = \%%{
  block_end_string = }
  variable_start_string = \*{
  variable_end_string = }
  comment_start_string = \#{
  comment_end_string = }

