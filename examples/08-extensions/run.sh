#!/bin/sh

SCRIPT_DIR=$(dirname "$0")

. "$SCRIPT_DIR/../../.venv/bin/activate"

python "$SCRIPT_DIR/../../templator.py" \
    run \
    --dump \
    --extension extension \
    --extension-path "$SCRIPT_DIR" \
    --extension-argument "example:test=TEST" \
    "$SCRIPT_DIR/template.rst.j2" \
    "$SCRIPT_DIR/context.yml"

deactivate
