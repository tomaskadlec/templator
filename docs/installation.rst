Installation
============

.. _virtualenv: https://virtualenv.pypa.io/en/stable/

Requirements
------------

  * Python 3 (tested with 3.5.x)
  * virtualenv_

Instructions
------------

The tool uses virtualenv_ to create a runtime environment.

  #. Install virtualenv_ through package manager of your operating system.
  #. Create virtualenv in the project directory. ::

      cd path/to/project
      virutalenv -p /path/to/python3 .venv

  #. Activate virtualenv. ::

      source .venv/bin/activate

  #. Download project dependencies. ::

      pip install -r requiremnts.txt

  #. (optional) If you have a directory listed in ``$PATH`` place a symlink to
     ``templator.sh`` to this directory and call it just ``templator``. This
     wrapper script will handle virtualenv_ activation/deactivation automatically.