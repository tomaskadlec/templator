#!/bin/sh

SCRIPT_DIR=$(dirname "$0")


. "$SCRIPT_DIR/../../.venv/bin/activate"
python "$SCRIPT_DIR/../../templator.py" run --no-reeval --dump "$SCRIPT_DIR/template.rst.j2" "$SCRIPT_DIR/context.yml" # > "/tmp/07-complex_strings.rst"

deactivate
