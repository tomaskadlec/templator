Commands
========

.. _run command: run.rst
.. _tree command: tree.rst

This section documents each available command.

  #. Single stream output - `run command`_
  #. Tree output - `tree command`_
