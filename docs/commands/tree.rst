Tree output
===========

.. _templator: https://gitlab.com/tomaskadlec/templator
.. _`depth-first search`: https://en.wikipedia.org/wiki/Depth-first_search
.. _Jinja 2: http://jinja.pocoo.org/docs/dev/

::

    Usage: templator.py tree [OPTIONS] BUILD_DIR TEMPLATE CONTEXTS...

      Outputs a tree structure of templates rendered with contexts to build_dir.

      CONTEXTS is one or more files/directories. If a directory is given it is
      searched for *yml files recursively. All files are loaded in the same
      order as they appear on the commandline or based on their order in
      directories.

    Options:
      --config TEXT                   Path to configuration file
      --dump / --no-dump              Dump context to STDERR
      --expressions / --no-expressions
                                      Parse Jinja2 expressions in context or not
      --schema TEXT                   Validate resulting context using pykwalify
                                      schema. May be provided multiple times.
      --scope-root TEXT               If given attributes are scoped using
                                      directory path left between scope root and
                                      file.
      --root TEXT                     Set context root
      --branching-attribute <TEXT TEXT>...
                                      Name of attribute that is used to branching
      --ordering-attribute TEXT       Name of attribute used to order dictionaries
      --help                          Show this message and exit.

Preprocessing
-------------

It is necessary to preprocess data here. Templator_ does not know anything about data structure.
In particular, it does not know if node has children or not. You must provide ``--branching-attribute``
option which expects ``name`` and ``value``. If such attribute is found and has expected
value it is considered as a branching node that may have children. Data is traversed using
such nodes than. Option is required and may be given multiple times.

There are several ways how to traverse the tree. Templator_ uses `depth-first search`_ algorithm.
Thus result of preprocessing is a list of nodes in the order they were visited. During preprocessing
several values are set to each node:

``__key``
    An attribute name under which is a node stored in the tree.

``__path``
    A path to from the tree root to a node (without node itself)

``__parent``
    A reference to the parent node.

``__children``
    A list of references to children of a node.

``__next``
    A reference to the next node (regarding the order nodes appeared in
    depth-first search). Might be ``None`` (last leaf).

``__previous``
    A reference to the previous node (regarding the order nodes appeared in
    depth-first search). Might be ``None`` (tree root).

The result allows to easily traverse the structure even from a `Jinja 2`_ template. The resulting
list may look as follows: ::

  [ OrderedDict, ...
    |- __parent = None
    |- __prev = None
    |- __next = OrderedDict{__key: 'first', ...}
    |- __children = [ OrderedDict{__key: 'first', ...}, ... ]
    |- __key = 'root'
    |- __path = '/'
    \- ... actual data ..

    ..., OrderedDict, ... ]
        |- __parent = OrderedDict{__key='root', ...}
        |- __prev = OrderedDict{__key='second', ...}
        |- __next = OrderedDict{__key: 'fourth', ...}
        |- __children = None
        |- __key = 'third'
        |- __path = '/root'
        \- ... actual data ..


Templating is called for each node in the resulting list. The same template is used for
rendering nodes (you may use it to ``include`` another one, e.g. based on the type
of the node). Template receives a dict with following keys:

``node``
  Current node

``context``
  The whole context

``extension``
  Extension used (guessed from template filename)

Filters
-------

There are special filters that can be used just for this command (e.g. filters will
probably fail if used with another command).

``relpath(node_to, node_from)``
    Returns relative path from one node to another. ::

      {# example usage - relative directory path to parrent #}
      {{ node['__parent']|relpath(node) }}/{{ node['__parent']['__key'] }}.{{ extension }}

``curpath(node)``
    Returns path to the current node from the tree root. Path is expressed as a list
    of its predecessors. The list starts with the tree root. ::

      {# example usage - simple breadcrumbs #}
      {% for element in node|curpath %}
        <a href="{{ element|relpath(node) }}/{{ element['__key'] }}.{{ extension }}">
            {{ element['__key'] }}
        </a>
      {% endfor %}

Macros
------

Macros are also available for the ``tree`` command. If you want to use them you must
import them. ::

  {% import 'macro/tree.j2' %}

``toc(context, node, title='__key', extension='html')``
  Generates table of contents in HTML format (``ol``\s). It is based on ``context``. Current
  ``node`` is used to check which node must not be a active link. ``title`` is the name of
  attribute that contain title to display (otherwise ``__key`` is used). Extension is just
  an extension used to generate URLs.
  
Examples
--------

Examples related to Tree output:

  * `06 - Tree output <../../examples/06-tree_output/README.rst>`_
