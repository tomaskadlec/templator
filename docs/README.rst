Documentation
=============

.. _templator: https://gitlab.com/tomaskadlec/templator

This is a index page of templator_ documentation.

.. _installation instructions: installation.rst
.. _invocation: invocation.rst
.. _structured data: data.rst
.. _authoring templates: templating.rst
.. _run command: commands/run.rst
.. _tree command: commands/tree.rst

Please refer to the following documents for further information:

  #. `Installation instructions`_
  #. `Invocation`_ of templator_
  #. Preparing `Structured data`_
  #. `Authoring templates`_
  #. Commands

     #. Single stream output - `run command`_
     #. Tree output - `tree command`_

.. _examples: ../examples/README.rst