#!/bin/sh

SCRIPT_DIR=$(dirname "$0")


. "$SCRIPT_DIR/../../.venv/bin/activate"
python "$SCRIPT_DIR/../../templator.py" run --dump --scope-root context -- "$SCRIPT_DIR/template.rst.j2" "$SCRIPT_DIR/context"  "$SCRIPT_DIR/context.yml" > "/tmp/05-scoped_attributes.rst"

deactivate
